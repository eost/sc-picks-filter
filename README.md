SCPFILTER
=========

`scpfilter` is a seiscomp3 application which aims to filter incoming picks. If
too many picks are received, `scpfilter` doesn't send them to the next
messaging group. It should be placed between `scautopick` and `scautolock`.

Installation
------------

1. Synchronise the source into seiscomp3 folder:

    ```bash
    rsync -av seiscomp3/ /path/to/seiscomp3/
    ```

2. Configure `scpfilter`. Make it listen `scautopick` and send message on the
`PICK` group (or equivalent).

3. Activate the seiscomp3 module:

    ```bash
    seiscomp enable scpfilter
    seiscomp start scpfilter
    ```
